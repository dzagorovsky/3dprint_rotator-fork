###3D-Print Showcase

This code was developed to run on an Arduino Uno using the [Adafruit Motor Shield](http://www.adafruit.com/products/1438) and a stepper motor.

Single switch input determines the run mode.  Possible options are:

* Continuous Motion
* Incremental Motion

####Continuous
This mode rotates the platter 360 degrees repeatedly.  Good for video capture of prints.
####Incremental
This mode rotates the platter 45 degrees, stops for roughly 5 seconds, and repeats.  This mode is good for still image captures.  Using this mode is good for creating GIFs or montages.


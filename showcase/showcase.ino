#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_PWMServoDriver.h"

#define speed 10 // 10 rpm

const int modePin = 2;

int mode = 0;

// Create Motor Shield object with default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
// Or, create it with a different I2C address (say for stacking)
// Adafruit_MotorShield AFMS = Adafruit_MotorShield(0x61); 

// Connect a stepper motor with 200 steps per revolution (1.8 degree)
// to motor port #2 (M3 and M4)
Adafruit_StepperMotor *stepper = AFMS.getStepper(200, 2);

void setup() {
  
  pinMode(modePin, INPUT);
  
  Serial.begin(9600);
  
  AFMS.begin();  // create with the default frequency 1.6KHz
  //AFMS.begin(1000);  // OR with a different frequency, say 1KHz
  
  stepper->setSpeed(speed);
}

void loop() {
  //////////////////////////////////////////////////////
  // Mode - Continuous rotation or incremental rotation
  // 0 - Continuous
  // 1 - Incremental
  //////////////////////////////////////////////////////
  mode = digitalRead(modePin);
  Serial.println(mode);
  
  if(mode == 0) {
    // Continuous logic
    Serial.println("Continuous Mode");
    stepper->step(200, FORWARD, MICROSTEP);
    
  }
  else if(mode == 1){
    // Incremental logic 
    Serial.println("Incremental Mode");
    
    Serial.println("Advancing 45 degree");
    stepper->step(25, FORWARD, MICROSTEP);    
    
    delay(5000);

  }
}

